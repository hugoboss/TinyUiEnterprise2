package org.tinygroup.action.data;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller

public class dataAction {
    @RequestMapping("/flow/cc")
    public String cc(Model model){
        int pId;
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        for (int i=0;i<10;i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("id", 10+i);
            if(i==0){
                pId=0;
            }else{
                pId=10+i%3;
            }
            map.put("pId", pId);
            map.put("name", "节点-"+i);
            list.add(map);
        }
        model.addAttribute("list", list);
        return "page/cc.page";
    }

}

